# SPDX-FileCopyrightText: 2019-2022 Pleroma Authors
# SPDX-License-Identifier: LGPL-3.0-only

# Used by "mix format"
[
  inputs: ["{mix,.formatter}.exs", "{config,lib,test}/**/*.{ex,exs}"]
]
